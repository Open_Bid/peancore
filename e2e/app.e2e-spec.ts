import { Pean2Page } from './app.po';

describe('pean2 App', function() {
  let page: Pean2Page;

  beforeEach(() => {
    page = new Pean2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
